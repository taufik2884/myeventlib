## Golang event publisher library
### To run example, copy your TLSTrustCertsFile "ca.cert.pem" file to example directory
##### Run example
```bash
cd example
go run . example
```

##### Run delayed
```bash
cd example
go run . delayed
```

##### Run readconfig
```bash
cd example
go run . readconfig
```

##### Run sendjson
```bash
cd example
go run . sendjson
```

##### Run logrus
```bash
cd example
go run . logrus
```