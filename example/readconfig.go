package main

import (
	"encoding/json"
	"log"
	"os"
	"os/signal"
	"syscall"

	eventlib "gitlab.com/taufik2884/myeventlib"
	clientlib "gitlab.com/taufik2884/myeventlib/client"
	cm "gitlab.com/taufik2884/myeventlib/common"
	pb "gitlab.com/taufik2884/myeventlib/pb"
	"google.golang.org/protobuf/proto"
)

func RunReadConfig() {
	// create client base on configuration from file
	client, err := eventlib.NewClientFromConfigFile("config.yaml")
	if err != nil {
		log.Fatal(err)
	}

	publisher, err := client.CreatePublisher("test_system", TOPIC, TOPIC2)
	if err != nil {
		log.Fatal(err)
	}

	defer func() {
		publisher.Close()
		client.Close()
	}()

	payload, err := proto.Marshal(&pb.TestPaylodMessage{
		Foo: "foo",
		Bar: "bar",
	})
	if err != nil {
		log.Fatal(err)
	}

	msg := &pb.EventMessageV1{
		ActivitySource: "tester",
		AssetId:        "backend",
		Timestamp:      cm.NewTimestamp(),
		Identity:       "08881850520",
		PayloadType:    "application/protobuf",
		Payload:        payload,
	}

	err = publisher.Publish(TOPIC, msg, nil)
	if err != nil {
		log.Fatal(err)
	}

	// with json payload
	js := map[string]interface{}{"foo": "foo", "bar": "bar"}
	payload, err = json.Marshal(js)
	if err != nil {
		log.Fatal(err)
	}

	msg = &pb.EventMessageV1{
		ActivitySource: "tester",
		AssetId:        "backend",
		Timestamp:      cm.NewTimestamp(),
		Identity:       "08881850520",
		PayloadType:    "application/json",
		Payload:        payload,
	}

	err = publisher.Publish(TOPIC, msg, nil)
	if err != nil {
		log.Fatal(err)
	}

	// Create subscriber
	subscriber := client.CreateSubscriber("tester")

	subscriber.Subscribe(TOPIC, clientlib.Shared, false, MessageHandler)

	go subscriber.Start()

	terminate := make(chan os.Signal, 1)
	signal.Notify(terminate, os.Interrupt, syscall.SIGTERM)
	<-terminate
}
