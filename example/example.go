package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"

	eventlib "gitlab.com/taufik2884/myeventlib"
	clientlib "gitlab.com/taufik2884/myeventlib/client"
	cm "gitlab.com/taufik2884/myeventlib/common"
	pb "gitlab.com/taufik2884/myeventlib/pb"

	"google.golang.org/protobuf/proto"
)

const (
	TOPIC      string = "persistent://tester/kates/test-1"
	TOPIC2     string = "persistent://tester/kates/test-2"
	TOKEN      string = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJrYXRlcyJ9.R62abM9cGZrcYdi91Mpxp1xfs1j8uAmCMY8PPrV2_8o"
	HOST       string = "pulsar+ssl://pulsar01-stg.smartfren.com:6651"
	CACERTFILE string = "ca.cert.pem"
)

var (
	EventClient clientlib.Client
	msgCount    int32
)

func main() {
	args := os.Args
	if len(args) > 1 {
		switch args[1] {
		case "delayed":
			RunReadDelayedDelivery()
		case "sendjson":
			RunPublisAsJSON()
		case "readconfig":
			RunReadConfig()
		case "logrus":
			RunWithLogrusLogger()
		default:
			Run()
		}
	}
}

func PrintMsg(msgId string, msg *pb.EventMessageV1, payload interface{}) {
	log.Println(fmt.Sprintf("Incoming message...\nMsgId: %s\nAcivity Source: %s\nAssetId: %s\nTimestamp: %v\nIdentity: %s\nPayloadType: %s\nPayload: %v\nCounter:%d\n",
		msgId, msg.ActivitySource, msg.AssetId, msg.Timestamp.AsTime(), msg.Identity, msg.PayloadType, payload, msgCount))
}

func MessageHandler(msgId string, msg []byte, Ack func(ok bool)) {
	message := &pb.EventMessageV1{}
	if err := proto.Unmarshal(msg, message); err != nil {
		log.Println("Error parsing message...", err)
		Ack(false)
		return
	}

	var parsedPayload interface{}
	if message.GetPayload() != nil {
		switch message.PayloadType {
		case "application/json":
			payload := make(map[string]interface{})
			if err := json.Unmarshal(message.GetPayload(), &payload); err != nil {
				log.Println("Error message payload...", err)
				Ack(false)
				return
			}
			parsedPayload = payload
		default: // application/protobuf   payload using pb.TestPaylodMessage
			payload := &pb.TestPaylodMessage{}
			if err := proto.Unmarshal(message.GetPayload(), payload); err != nil {
				log.Println("Error message payload...", err)
				Ack(false)
				return
			}
			parsedPayload = payload
		}
	}
	msgCount++
	PrintMsg(msgId, message, parsedPayload)

	Ack(true)
}

func Run() {
	var err error
	cfg := &cm.Configuration{
		ClientType:          "pulsar",
		Host:                HOST,
		CertFile:            CACERTFILE,
		AuthenticationToken: TOKEN,
	}

	EventClient, err = eventlib.NewClient(cfg)
	if err != nil {
		log.Fatal(err)
	}

	publisher, err := EventClient.CreatePublisher("test_system", TOPIC, TOPIC2)
	if err != nil {
		log.Fatal(err)
	}

	defer func() {
		publisher.Close()
		EventClient.Close()
	}()

	// send message
	go func() {
		payload, err := proto.Marshal(&pb.TestPaylodMessage{
			Foo: "foo",
			Bar: "bar",
		})
		if err != nil {
			log.Fatal(err)
		}

		msg := &pb.EventMessageV1{
			ActivitySource: "tester",
			AssetId:        "backend",
			Timestamp:      cm.NewTimestamp(),
			Identity:       "08881850520",
			PayloadType:    "application/protobuf",
			Payload:        payload,
		}

		err = publisher.Publish(TOPIC, msg, nil)
		if err != nil {
			log.Fatal(err)
		}
	}()

	// send with json payload
	go func() {
		js := map[string]interface{}{"foo": "foo", "bar": "bar"}
		payload, err := json.Marshal(js)
		if err != nil {
			log.Fatal(err)
		}

		for i := 0; i < 10; i++ {
			msg := &pb.EventMessageV1{
				ActivitySource: "tester",
				AssetId:        "backend",
				Timestamp:      cm.NewTimestamp(),
				Identity:       "08881850520",
				PayloadType:    "application/json",
				Payload:        payload,
			}

			err = publisher.Publish(TOPIC2, msg, nil)
			if err != nil {
				log.Fatal(err)
			}
			//time.Sleep(3 * time.Second)
		}
	}()

	// Create subscriber
	subscriber := EventClient.CreateSubscriber("tester")

	subscriber.Subscribe(TOPIC, clientlib.Shared, false, MessageHandler)
	subscriber.Subscribe(TOPIC2, clientlib.Shared, false, MessageHandler)

	go subscriber.Start()

	terminate := make(chan os.Signal, 1)
	signal.Notify(terminate, os.Interrupt, syscall.SIGTERM)
	<-terminate
}
