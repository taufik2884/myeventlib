package main

import (
	"bytes"
	"encoding/json"
	"log"
	"os"
	"os/signal"
	"syscall"

	"github.com/gogo/protobuf/jsonpb"
	eventlib "gitlab.com/taufik2884/myeventlib"
	clientlib "gitlab.com/taufik2884/myeventlib/client"
	cm "gitlab.com/taufik2884/myeventlib/common"
	pb "gitlab.com/taufik2884/myeventlib/pb"
	"google.golang.org/protobuf/proto"
)

func MessageJSONHandler(msgId string, msg []byte, Ack func(ok bool)) {
	message := &pb.EventMessageV1{}
	r := bytes.NewReader(msg)
	if err := jsonpb.Unmarshal(r, message); err != nil {
		log.Println("Error parsing message...", err)
		Ack(false)
		return
	}

	var parsedPayload interface{}
	if message.GetPayload() != nil {
		switch message.PayloadType {
		case "application/json":
			payload := make(map[string]interface{})
			if err := json.Unmarshal(message.GetPayload(), &payload); err != nil {
				log.Println("Error message payload...", err)
				Ack(false)
				return
			}
			parsedPayload = payload
		default: // application/protobuf   payload using pb.TestPaylodMessage
			payload := &pb.TestPaylodMessage{}
			if err := proto.Unmarshal(message.GetPayload(), payload); err != nil {
				log.Println("Error message payload...", err)
				Ack(false)
				return
			}
			parsedPayload = payload
		}
	}
	msgCount++
	PrintMsg(msgId, message, parsedPayload)

	Ack(true)
}

// publishing protobuf message as json, so the client can parse with json.Unmarshal
func RunPublisAsJSON() {
	cfg := &cm.Configuration{
		ClientType:          "pulsar",
		Host:                HOST,
		CertFile:            CACERTFILE,
		AuthenticationToken: TOKEN,
	}

	client, err := eventlib.NewClient(cfg)
	if err != nil {
		log.Fatal(err)
	}

	publisher, err := client.CreatePublisher("test_system", TOPIC, TOPIC2)
	if err != nil {
		log.Fatal(err)
	}

	defer func() {
		publisher.Close()
		client.Close()
	}()

	payload, err := proto.Marshal(&pb.TestPaylodMessage{
		Foo: "foo",
		Bar: "bar",
	})
	if err != nil {
		log.Fatal(err)
	}

	msg := &pb.EventMessageV1{
		ActivitySource: "tester",
		AssetId:        "backend",
		Timestamp:      cm.NewTimestamp(),
		Identity:       "08881850520",
		PayloadType:    "application/protobuf",
		Payload:        payload,
	}

	err = publisher.PublishJSON(TOPIC, msg, nil)
	if err != nil {
		log.Fatal(err)
	}

	// with json payload
	js := map[string]interface{}{"foo": "foo", "bar": "bar"}
	payload, err = json.Marshal(js)
	if err != nil {
		log.Fatal(err)
	}

	msg = &pb.EventMessageV1{
		ActivitySource: "tester",
		AssetId:        "backend",
		Timestamp:      cm.NewTimestamp(),
		Identity:       "08881850520",
		PayloadType:    "application/json",
		Payload:        payload,
	}

	err = publisher.PublishJSON(TOPIC, msg, nil)
	if err != nil {
		log.Fatal(err)
	}

	// Create subscriber
	subscriber := client.CreateSubscriber("tester")

	subscriber.Subscribe(TOPIC, clientlib.Shared, false, MessageJSONHandler)

	go subscriber.Start()

	terminate := make(chan os.Signal, 1)
	signal.Notify(terminate, os.Interrupt, syscall.SIGTERM)
	<-terminate
}
