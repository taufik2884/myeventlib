package main

import (
	"encoding/json"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"

	eventlib "gitlab.com/taufik2884/myeventlib"
	clientlib "gitlab.com/taufik2884/myeventlib/client"
	cm "gitlab.com/taufik2884/myeventlib/common"
	pb "gitlab.com/taufik2884/myeventlib/pb"
	"google.golang.org/protobuf/proto"
)

func MessageDelayedHandler(msgId string, msg []byte, Ack func(ok bool)) {
	message := &pb.EventMessageV1{}
	if err := proto.Unmarshal(msg, message); err != nil {
		log.Println("Error parsing message...", err)
		Ack(false)
		return
	}

	var parsedPayload interface{}
	if message.GetPayload() != nil {
		switch message.PayloadType {
		case "application/json":
			payload := make(map[string]interface{})
			if err := json.Unmarshal(message.GetPayload(), &payload); err != nil {
				log.Println("Error message payload...", err)
				Ack(false)
				return
			}
			parsedPayload = payload
		default: // application/protobuf   payload using pb.TestPaylodMessage
			payload := &pb.TestPaylodMessage{}
			if err := proto.Unmarshal(message.GetPayload(), payload); err != nil {
				log.Println("Error message payload...", err)
				Ack(false)
				return
			}
			parsedPayload = payload
		}
	}
	msgCount++
	PrintMsg(msgId, message, parsedPayload)

	Ack(true)
}

func RunReadDelayedDelivery() {
	cfg := &cm.Configuration{
		ClientType:          "pulsar",
		Host:                HOST,
		CertFile:            CACERTFILE,
		AuthenticationToken: TOKEN,
	}
	var err error

	EventClient, err = eventlib.NewClient(cfg)
	if err != nil {
		log.Fatal(err)
	}

	publisher, err := EventClient.CreatePublisher("test_system", TOPIC, TOPIC2)
	if err != nil {
		log.Fatal(err)
	}

	defer func() {
		publisher.Close()
		EventClient.Close()
	}()

	payload, err := proto.Marshal(&pb.TestPaylodMessage{
		Foo: "foo",
		Bar: "bar",
	})
	if err != nil {
		log.Fatal(err)
	}

	msg := &pb.EventMessageV1{
		ActivitySource: "tester",
		AssetId:        "backend",
		Timestamp:      cm.NewTimestamp(),
		Identity:       "08881850520",
		PayloadType:    "application/protobuf",
		Payload:        payload,
	}

	// send with 5 second delay
	err = publisher.Publish(TOPIC, msg, nil, time.Now().Add(5*time.Second))
	if err != nil {
		log.Fatal(err)
	}

	// send delayed message 3 times, 5s delay, 10s delay and 15s delay
	err = publisher.Publish(TOPIC, msg, nil, time.Now().Add(5*time.Second), time.Now().Add(10*time.Second), time.Now().Add(15*time.Second))
	if err != nil {
		log.Fatal(err)
	}

	// Create subscriber
	subscriber := EventClient.CreateSubscriber("tester")

	subscriber.Subscribe(TOPIC, clientlib.Shared, false, MessageHandler)

	go subscriber.Start()

	terminate := make(chan os.Signal, 1)
	signal.Notify(terminate, os.Interrupt, syscall.SIGTERM)
	<-terminate
}
