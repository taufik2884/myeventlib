package eventlib

import (
	"reflect"

	client "gitlab.com/taufik2884/myeventlib/client"
	cm "gitlab.com/taufik2884/myeventlib/common"
	pulsarlib "gitlab.com/taufik2884/myeventlib/internal/client/pulsar"
	solacelib "gitlab.com/taufik2884/myeventlib/internal/client/solace"
)

// pulsar is default messaging platform
func getClientInstance(clientType string) client.Client {
	var (
		p            client.Client
		clientStruct = map[string]reflect.Type{
			"pulsar": reflect.TypeOf(&pulsarlib.Client{}),
			"solace": reflect.TypeOf(&solacelib.Client{}),
		}
	)

	if t, ok := clientStruct[clientType]; ok {
		v := reflect.New(t.Elem())
		p = v.Interface().(client.Client)
	} else {
		p = &pulsarlib.Client{}
	}
	return p
}

func NewClient(cfg *cm.Configuration) (client.Client, error) {
	// set AllowInsecureConnection to true to skip golang issue GODEBUG=x509ignoreCN=0
	cfg.AllowInsecureConnection = true
	p := getClientInstance(cfg.ClientType)
	if err := p.Connect(cfg); err != nil {
		return nil, err
	}

	return p, nil
}

func NewClientFromConfigFile(ymlFile string) (client.Client, error) {
	cfg, err := cm.ReadConfig(ymlFile)
	if err != nil {
		return nil, err
	}

	return NewClient(cfg)
}
