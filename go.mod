module gitlab.com/taufik2884/myeventlib

go 1.16

require (
	github.com/apache/pulsar-client-go v0.5.0
	github.com/gogo/protobuf v1.3.2
	github.com/golang/protobuf v1.5.2
	github.com/google/uuid v1.2.0
	github.com/rs/zerolog v1.23.0
	github.com/sirupsen/logrus v1.4.2
	go.uber.org/zap v1.17.0
	google.golang.org/protobuf v1.26.0
	gopkg.in/yaml.v2 v2.4.0
)
