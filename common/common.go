package common

import (
	"io/ioutil"

	timestamppb "google.golang.org/protobuf/types/known/timestamppb"
	"gopkg.in/yaml.v2"
)

type (
	Configuration struct {
		ClientType              string `yaml:"client_type"` //pulsar, solace, etc
		Host                    string `yaml:"host"`
		CertFile                string `yaml:"cert_file"`
		AuthenticationToken     string `yaml:"authentication_token"`
		AllowInsecureConnection bool   `yaml:"allow_insecure_connection"`
	}
)

func NewTimestamp() *timestamppb.Timestamp {
	return timestamppb.Now()
}

func ReadConfig(filename string) (*Configuration, error) {
	dat, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	config := &Configuration{}
	err = yaml.Unmarshal(dat, config)
	return config, err
}
