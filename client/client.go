package client

import (
	"errors"
	"time"

	cm "gitlab.com/taufik2884/myeventlib/common"
	pb "gitlab.com/taufik2884/myeventlib/pb"
)

type (
	Client interface {
		Connect(cfg *cm.Configuration) error
		GetClientType() string
		CreatePublisher(name string, topics ...string) (Publisher, error)
		CreateSubscriber(name string) Subscriber
		Close() error
	}

	Publisher interface {
		GetClient() Client

		// Publishing message as protobuf, the consumer have to parse with proto.Unmarshal.
		// Note: "Delayed message delivery only works well in Shared subscription mode.
		// In Exclusive and Failover subscription mode, the delayed message is dispatched immediately."
		Publish(topic string, msg *pb.EventMessageV1, props map[string]string, deliverAt ...time.Time) error

		// Publishing message as json, the consumer will parse with json.Unmarshal.
		// Note: "Delayed message delivery only works well in Shared subscription mode.
		// In Exclusive and Failover subscription mode, the delayed message is dispatched immediately."
		PublishJSON(topic string, msg *pb.EventMessageV1, props map[string]string, deliverAt ...time.Time) error

		Close() error
	}

	Subscriber interface {
		Subscribe(topic string, subscriptionType SubscriptionType, autoAck bool, handler ConsumerHandler) error
		Start()
		Close() error
	}

	ConsumerHandler func(msgId string, msg []byte, Ack func(ok bool))

	SubscriptionType int
)

const (
	Exclusive SubscriptionType = iota
	Shared
)

func ValidateMessage(msg *pb.EventMessageV1) error {
	errMsg := "Need required field "
	if msg.GetActivitySource() == "" {
		return errors.New(errMsg + "ActivitySource")
	}

	if msg.GetAssetId() == "" {
		return errors.New(errMsg + "AssetId")
	}

	if msg.GetIdentity() == "" {
		return errors.New(errMsg + "Identity")
	}
	if msg.GetTimestamp().AsTime().IsZero() {
		return errors.New(errMsg + "Timestamp")
	}

	if msg.GetPayload() == nil {
		return errors.New(errMsg + "Payload")
	}

	return nil
}
