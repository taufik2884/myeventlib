package solacelib

import (
	"bytes"
	"fmt"
	"time"

	"gitlab.com/taufik2884/myeventlib/client"
	cm "gitlab.com/taufik2884/myeventlib/common"
	pb "gitlab.com/taufik2884/myeventlib/pb"

	"github.com/gogo/protobuf/jsonpb"
	"google.golang.org/protobuf/proto"
)

type (
	publisher struct {
		ClientType string
		Client     client.Client
	}
)

func (p *publisher) Init(cfg *cm.Configuration) error {
	return nil
}

func (p *publisher) Publish(topic string, msg *pb.EventMessageV1, props map[string]string, deliverAt ...time.Time) error {
	msgBytes, err := proto.Marshal(msg)
	if err != nil {
		return err
	}

	return p.send(topic, msgBytes, props, deliverAt...)
}

func (p *publisher) PublishJSON(topic string, msg *pb.EventMessageV1, props map[string]string, deliverAt ...time.Time) error {
	var msgBytes bytes.Buffer
	m := jsonpb.Marshaler{}
	err := m.Marshal(&msgBytes, msg)
	if err != nil {
		return err
	}

	return p.send(topic, msgBytes.Bytes(), props, deliverAt...)
}

func (p *publisher) send(topic string, msg []byte, props map[string]string, deliverAt ...time.Time) error {
	fmt.Println("Publishing message...", string(msg))
	return nil
}

func (p *publisher) GetClient() client.Client {
	return p.Client
}

func (p *publisher) Close() error {
	return nil
}
