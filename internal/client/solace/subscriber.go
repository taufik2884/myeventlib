package solacelib

import (
	"fmt"

	client "gitlab.com/taufik2884/myeventlib/client"
)

type (
	subscriber struct {
		Name   string
		Client *Client
	}
)

/// Subscriber ///
func (s *subscriber) Subscribe(topic string, subscriptionType client.SubscriptionType, autoAck bool, callback client.ConsumerHandler) error {
	return nil
}

func (s *subscriber) Start() {
	fmt.Println("Consumer Started....")
}

func (s *subscriber) Close() error {
	return nil
}
