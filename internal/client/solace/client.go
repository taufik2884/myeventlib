package solacelib

import (
	client "gitlab.com/taufik2884/myeventlib/client"
	cm "gitlab.com/taufik2884/myeventlib/common"
)

type (
	Client struct {
		ClientType string
	}
)

func (c *Client) Connect(cfg *cm.Configuration) error {
	return nil
}

func (c *Client) GetClientType() string {
	return c.ClientType
}

func (c *Client) GetClient() client.Client {
	return nil
}

func (c *Client) CreatePublisher(publisherName string, topics ...string) (client.Publisher, error) {
	return &publisher{}, nil
}

func (c *Client) CreateSubscriber(name string) client.Subscriber {
	return nil
}

func (c *Client) Close() error {
	return nil
}
