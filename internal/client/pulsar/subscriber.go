package pulsarlib

import (
	"fmt"

	"github.com/apache/pulsar-client-go/pulsar"
	client "gitlab.com/taufik2884/myeventlib/client"
)

type (
	consumerContext struct {
		Consumer pulsar.Consumer
		Handler  client.ConsumerHandler
		AutoAck  bool
	}
	subscriber struct {
		Name      string
		Client    *Client
		Consumers map[string]*consumerContext
		Channel   chan pulsar.ConsumerMessage
	}
)

/// Subscriber ///
func (s *subscriber) Subscribe(topic string, subscriptionType client.SubscriptionType, autoAck bool, callback client.ConsumerHandler) error {
	options := pulsar.ConsumerOptions{
		Topic:            topic,
		SubscriptionName: s.Name,
		Type:             pulsar.SubscriptionType(subscriptionType),
		MessageChannel:   s.Channel,
	}

	consumer, err := s.Client.CreateConsumer(options)
	if err != nil {
		return err
	}

	s.Consumers[topic] = &consumerContext{
		Consumer: consumer,
		AutoAck:  autoAck,
		Handler:  callback,
	}

	return nil
}

func (s *subscriber) Start() {
	defer s.Close()

	for cm := range s.Channel {
		msg := cm.Message
		if msg == nil {
			break
		}

		topic := msg.Topic()
		if cc, found := s.Consumers[topic]; found {
			acknowledge := func(ok bool) {
				if !cc.AutoAck {
					if ok {
						cc.Consumer.Ack(msg)
					} else {
						cc.Consumer.Nack(msg)
					}
				}
			}

			// callback msg to handler
			go cc.Handler(fmt.Sprintf("%v", msg.ID()), msg.Payload(), acknowledge)

			if cc.AutoAck {
				cc.Consumer.Ack(msg)
			}
		}
	}

	//fmt.Println("Channel closed")
}

func (s *subscriber) Close() error {
	// stop the listener
	s.Channel <- pulsar.ConsumerMessage{}

	for _, c := range s.Consumers {
		c.Consumer.Close()
	}

	return nil
}
