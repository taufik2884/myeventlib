package pulsarlib

import (
	"github.com/apache/pulsar-client-go/pulsar"
	"github.com/google/uuid"
	client "gitlab.com/taufik2884/myeventlib/client"
	cm "gitlab.com/taufik2884/myeventlib/common"
)

type (
	Client struct {
		ClientType  string
		Connection  pulsar.Client
		Publishers  []*publisher
		Subscribers []*subscriber
	}
)

func generateOption(cfg *cm.Configuration) pulsar.ClientOptions {
	opt := pulsar.ClientOptions{
		URL: cfg.Host,
	}

	if cfg.CertFile != "" {
		opt.TLSTrustCertsFilePath = cfg.CertFile
		opt.TLSAllowInsecureConnection = cfg.AllowInsecureConnection
	}

	if cfg.AuthenticationToken != "" {
		opt.Authentication = pulsar.NewAuthenticationToken(cfg.AuthenticationToken)
	}

	return opt
}

func (p *Client) Connect(cfg *cm.Configuration) error {
	opt := generateOption(cfg)

	client, err := pulsar.NewClient(opt)
	if err != nil {
		return err
	}

	p.ClientType = cfg.ClientType
	p.Connection = client

	return err
}

func (c *Client) GetClientType() string {
	return c.ClientType
}

func (c *Client) CreatePublisher(name string, topics ...string) (client.Publisher, error) {
	p := &publisher{
		Client:    c,
		Name:      name + "-" + uuid.New().String(),
		Producers: make(map[string]pulsar.Producer),
	}

	for _, topic := range topics {
		if producer, err := c.CreateProducer(pulsar.ProducerOptions{Topic: topic, Name: p.Name}); err == nil {
			p.Producers[topic] = producer
		} else {
			return nil, err
		}
	}

	c.Publishers = append(c.Publishers, p)

	return p, nil
}

func (c *Client) CreateSubscriber(name string) client.Subscriber {
	s := &subscriber{
		Client:    c,
		Name:      name,
		Consumers: make(map[string]*consumerContext),
		Channel:   make(chan pulsar.ConsumerMessage, 100),
	}
	c.Subscribers = append(c.Subscribers, s)

	return s
}

func (c *Client) CreateProducer(opt pulsar.ProducerOptions) (pulsar.Producer, error) {
	return c.Connection.CreateProducer(opt)
}

func (c *Client) CreateConsumer(opt pulsar.ConsumerOptions) (pulsar.Consumer, error) {
	return c.Connection.Subscribe(opt)
}

func (c *Client) Close() error {
	// for _, p := range c.Publishers {
	// 	p.Close()
	// }

	// for _, s := range c.Subscribers {
	// 	s.Close()
	// }

	c.Connection.Close()
	return nil
}
