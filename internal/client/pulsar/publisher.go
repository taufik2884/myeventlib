package pulsarlib

import (
	"bytes"
	"context"
	"time"

	client "gitlab.com/taufik2884/myeventlib/client"
	pb "gitlab.com/taufik2884/myeventlib/pb"

	"github.com/apache/pulsar-client-go/pulsar"

	"github.com/gogo/protobuf/jsonpb"
	"google.golang.org/protobuf/proto"
)

type (
	publisher struct {
		Client    *Client
		Name      string
		Producers map[string]pulsar.Producer
	}
)

/// Publisher ///
func (p *publisher) Publish(topic string, msg *pb.EventMessageV1, props map[string]string, deliverAt ...time.Time) error {
	if err := client.ValidateMessage(msg); err != nil {
		return err
	}

	msgBytes, err := proto.Marshal(msg)
	if err != nil {
		return err
	}

	return p.send(topic, msgBytes, props, deliverAt...)
}

func (p *publisher) PublishJSON(topic string, msg *pb.EventMessageV1, props map[string]string, deliverAt ...time.Time) error {
	if err := client.ValidateMessage(msg); err != nil {
		return err
	}

	var msgBytes bytes.Buffer
	m := jsonpb.Marshaler{}
	err := m.Marshal(&msgBytes, msg)
	if err != nil {
		return err
	}

	return p.send(topic, msgBytes.Bytes(), props, deliverAt...)
}

func (p *publisher) send(topic string, msg []byte, props map[string]string, deliverAt ...time.Time) error {
	var (
		prod pulsar.Producer
		err  error
		ok   bool
	)

	if prod, ok = p.Producers[topic]; !ok {
		prod, err = p.Client.CreateProducer(pulsar.ProducerOptions{Topic: topic, Name: p.Name})
		if err != nil {
			return err
		}

		p.Producers[topic] = prod
	}

	msgPayload := &pulsar.ProducerMessage{
		Payload:   msg,
		EventTime: time.Now(),
	}

	if props != nil {
		msgPayload.Properties = props
	}

	if len(deliverAt) > 0 {
		for _, t := range deliverAt {
			msgPayload.DeliverAt = t
			_, err = prod.Send(context.Background(), msgPayload)
			if err != nil {
				return err
			}
		}
		return err
	}

	_, err = prod.Send(context.Background(), msgPayload)
	return err
}

func (p *publisher) GetClient() client.Client {
	return p.Client
}

func (p *publisher) Close() error {
	for _, prod := range p.Producers {
		prod.Close()
	}
	return nil
}
